/*General Libraries*/
#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/devicetree.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/logging/log.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <zephyr/smf.h>
#include <zephyr/drivers/adc.h> 
#include <zephyr/drivers/pwm.h>
#include <zephyr/drivers/sensor.h>
#include <zephyr/sys/util_macro.h>

/*Bluetooth Libraries*/
#include <zephyr/bluetooth/bluetooth.h>
#include <zephyr/bluetooth/uuid.h>
#include <zephyr/bluetooth/gatt.h>
#include <zephyr/bluetooth/hci.h> // host controller interface
#include <zephyr/bluetooth/services/bas.h>  // battery service GATT
#include <zephyr/settings/settings.h>
/*NRFX Library*/
#include <nrfx_power.h>


#define SLEEP_TIME_MS   100
#define HEARTBEAT_ON_TIME_MS 1000.0
#define HEARTBEAT_MS 500
#define DUTY_CYCLE_MIN 0
#define DUTY_CYCLE_MAX 100
#define MEASURE_DURATION_MS 1000
#define SAMPLE_MAX_HZ 500
#define SAMPLE_MIN_HZ 100
#define SAMPLES_PER_CYCLE 10
#define CYCLE_MS 1000
#define CYCLE_USEC 1000000
#define MIN_SIZE 1000
#define MAX_SIZE 5000
#define MIN_VPP1_MV 5
#define MAX_VPP1_MV 50
#define MIN_VPP2_MV 10
#define MAX_VPP2_MV 150
#define MEASUREMENT_DELAY_MS 1000  // delay between measurements (is this pressure?)
#define MOTION_MEASUREMENT_DELAY_MS 5000 // delay between motion measurements
#define OVERSAMPLE 10  // number of samples to average together
#define SAMPLE_INTERVAL 10
#define LENGTH_OF_BLEARRAY 3
#define NOMINAL_BATT_MV 3600
#define TOTAL_SIZE      6000
#define ACCEL_ALIAS(i) DT_ALIAS(_CONCAT(accel, i))
#define ACCELEROMETER_DEVICE(i, _)                                                                 \
	IF_ENABLED(DT_NODE_EXISTS(ACCEL_ALIAS(i)), (DEVICE_DT_GET(ACCEL_ALIAS(i)),))
#define NUM_ACCEL_SENSORS 10 // Maximum is 10, can be changed depending on timing

#define BUTTON_EVENT BIT(0) // This will actually end up being NO_MOTION_DETECTED bit
#define STOP_INFLATION BIT(1) //american politics joke haha funny
#define FULL_DEFLATE BIT(2) //american politics joke haha funny


LOG_MODULE_REGISTER(main, LOG_LEVEL_DBG);



static const struct smf_state user_states[];
enum user_state { init, idle, accel, pressure_inflate, pressure_deflate, error }; // Remember to remove accel after testing

//I swear I tried to add in a .c and .h file, but it got mad at compile saying that I didn't define the 
//read pressure sensor function - so i said whatever i'll put it all here
struct Pressure {
    int32_t atm;  // baseline atm pressure before the seal
    int32_t buffer; // pressure buffer for the current measurement
    int pressure_curve_inflation[TOTAL_SIZE];
    int max_count;
} pressure_mmhg;

/* support up to 10 accelerometer sensors */
static const struct device *const sensors[] = {LISTIFY(NUM_ACCEL_SENSORS, ACCELEROMETER_DEVICE, ())};

static const enum sensor_channel channels[] = {
	SENSOR_CHAN_ACCEL_X,
	SENSOR_CHAN_ACCEL_Y,
	SENSOR_CHAN_ACCEL_Z,
};

// Struct to store accelerometer data
struct SensorData {
    double x;
    double y;
    double z;
};

struct SensorData accel_data[NUM_ACCEL_SENSORS];

struct s_object {
        struct smf_ctx ctx;

        struct k_event smf_event;
        int32_t events;
} state_obj;



static const struct gpio_dt_spec redled = GPIO_DT_SPEC_GET(DT_ALIAS(redled), gpios);
static const struct gpio_dt_spec greenled = GPIO_DT_SPEC_GET(DT_ALIAS(greenled), gpios);
static const struct gpio_dt_spec blueled = GPIO_DT_SPEC_GET(DT_ALIAS(blueled), gpios);
static const struct gpio_dt_spec button = GPIO_DT_SPEC_GET(DT_ALIAS(button), gpios);
static const struct gpio_dt_spec power2 = GPIO_DT_SPEC_GET(DT_ALIAS(power2), gpios);
static const struct gpio_dt_spec pump = GPIO_DT_SPEC_GET(DT_ALIAS(pump), gpios);
static const struct gpio_dt_spec solenoid1 = GPIO_DT_SPEC_GET(DT_ALIAS(solenoid1), gpios);
static const struct gpio_dt_spec solenoid2 = GPIO_DT_SPEC_GET(DT_ALIAS(solenoid2), gpios);

const struct device *const pressure_in = DEVICE_DT_GET_ONE(honeywell_mpr); 

/*Declare Timer Functions*/
void heartbeat_toggle(struct k_timer *heartbeat_timer);
void heartbeat_start(struct k_timer *heartbeat_timer);

void measure_pressure_start(struct k_timer *measure_pressure_timer);
void measure_deflate_start(struct k_timer *measure_deflate_timer);

void measure_motion_start(struct k_timer *motion_timer);

/*Declare Work Queue Handlers*/
void pressure_inflate_work_handler(struct k_work *pressure_inflate_work);
void pressure_deflate_work_handler(struct k_work *pressure_deflate_work);
void measure_motion_work_handler(struct k_work *measure_motion_work);

/* Declare Regular Functions */
int check_interfaces(void);
int check_gpio_interfaces(void);
int config_leds(void);
int config_buttons(void);
int read_pressure_sensor(const struct device *pressure_in, struct Pressure *pressure_mmhg);
static int print_accels(const struct device *dev, size_t index);
int get_accel_vals(void);


K_TIMER_DEFINE(heartbeat_timer, heartbeat_toggle, NULL);
K_TIMER_DEFINE(measure_pressure_timer, measure_pressure_start, NULL);
K_TIMER_DEFINE(measure_deflate_timer, measure_deflate_start, NULL);

K_TIMER_DEFINE(measure_motion_timer, measure_motion_start, NULL); // Interval to measure motion


K_WORK_DEFINE(pressure_inflate_work, pressure_inflate_work_handler);
K_WORK_DEFINE(pressure_deflate_work, pressure_deflate_work_handler);

K_WORK_DEFINE(measure_motion_work, measure_motion_work_handler); // Work for measuring motion



void measure_pressure_start(struct k_timer *measure_pressure_timer) {
	k_work_submit(&pressure_inflate_work);
}

void measure_motion_start(struct k_timer *motion_timer) {
        k_work_submit(&measure_motion_work);
}

void measure_deflate_start(struct k_timer *measure_pressure_timer) {
	k_work_submit(&pressure_deflate_work);
}

void pressure_inflate_work_handler(struct k_work *pressure_inflate_work) {
	int current_pressure = read_pressure_sensor(pressure_in, &pressure_mmhg);
	if (current_pressure == -9999) {
                LOG_ERR("Could not read pressure sensor.");
		k_event_post(&state_obj.smf_event, STOP_INFLATION);

        }
        LOG_INF("difference in pressure: %d", current_pressure);
	if ((pressure_mmhg.max_count>(MAX_SIZE-1)) || (current_pressure>= 160)) {
		LOG_INF("Stop Sampling");
		k_event_post(&state_obj.smf_event, STOP_INFLATION);
	}
	pressure_mmhg.max_count++;

}

void pressure_deflate_work_handler(struct k_work *pressure_deflate_work) {
	pressure_mmhg.pressure_curve_inflation[pressure_mmhg.max_count] = read_pressure_sensor(pressure_in, &pressure_mmhg);
	if (pressure_mmhg.pressure_curve_inflation[pressure_mmhg.max_count] == -9999) {
                LOG_ERR("Could not read pressure sensor.");
		k_event_post(&state_obj.smf_event, FULL_DEFLATE);

        }
        LOG_INF("difference in pressure: %d", pressure_mmhg.pressure_curve_inflation[pressure_mmhg.max_count]);
	if (pressure_mmhg.max_count >= (MAX_SIZE-1) || (pressure_mmhg.pressure_curve_inflation[pressure_mmhg.max_count] < 60) ) {
		LOG_INF("Fully deflate now");
		k_event_post(&state_obj.smf_event, FULL_DEFLATE);
	}
	pressure_mmhg.max_count++;

}

void measure_motion_work_handler(struct k_work *measure_motion_work) {
        // For now I will do nothing! But eventually I'll measure motion
        LOG_DBG("Measuring Motion...");
}


void button_pressed(const struct device *dev, struct gpio_callback *cb, uint32_t pins)
{
        LOG_INF("Button 1 ADC Measure Callback");
        k_event_post(&state_obj.smf_event, BUTTON_EVENT);
}

void heartbeat_toggle(struct k_timer *heartbeat_timer) {
        // LOG_INF("green led");

        gpio_pin_toggle_dt(&greenled);
}

static struct gpio_callback button_cb_data;

/**** STATE: Init ****/
static void init_entry(void *o) {
	LOG_INF("STATE: Init (Entry)");

    /* Check that all IO pins and interfaces are ready */
	int err = check_interfaces();
	if (err) {
		LOG_ERR("ADC interfaces not ready (err = %d)", err);
	}

        err = check_gpio_interfaces();
	if (err) {
		LOG_ERR("GPIO interfaces not ready (err = %d)", err);
	}

	err = config_leds();
	if (err) {
	        LOG_ERR("LED pin configuration error (%d).", err);
	}

	err = config_buttons();
	if (err) {
	        LOG_ERR("Button configuration error (%d).", err);
	}
}

static void init_run(void *o) {

        smf_set_state(SMF_CTX(&state_obj), &user_states[idle]);
}

static void init_exit(void *o) {

	k_timer_start(&heartbeat_timer, K_MSEC(HEARTBEAT_MS), K_MSEC(HEARTBEAT_MS));

}

/**** STATE: Idle ****/
static void idle_entry(void *o) {
	LOG_INF("Idle Entry");
        gpio_pin_set_dt(&greenled, 1);
        gpio_pin_set_dt(&blueled, 0);
        gpio_pin_set_dt(&power2, 0);
        gpio_pin_set_dt(&pump, 0);
        gpio_pin_set_dt(&solenoid1, 0);
        gpio_pin_set_dt(&solenoid2, 0);
	pressure_mmhg.max_count = 0;

        for (int i = 0; i<MAX_SIZE; i++) {
                pressure_mmhg.pressure_curve_inflation[i] = 0;
        }


        gpio_pin_interrupt_configure_dt(&button, GPIO_INT_EDGE_TO_ACTIVE);
	gpio_init_callback(&button_cb_data, button_pressed, BIT(button.pin));
	gpio_add_callback(button.port, &button_cb_data);

	/*Clear All Events Except BLE Event*/
	k_event_clear(&state_obj.smf_event, BUTTON_EVENT);
	k_event_clear(&state_obj.smf_event, STOP_INFLATION);
	k_event_clear(&state_obj.smf_event, FULL_DEFLATE);

        k_timer_start(&measure_motion_timer, K_MSEC(MOTION_MEASUREMENT_DELAY_MS), K_MSEC(MOTION_MEASUREMENT_DELAY_MS));
        
        /* This will not be in the final code, it is just for testing */
        //smf_set_state(SMF_CTX(&state_obj), &user_states[accel]);
}

static void idle_run(void *o) {

        /* Block until an event is detected */
        state_obj.events = k_event_wait(&state_obj.smf_event, BUTTON_EVENT, false, K_NO_WAIT);
        if (state_obj.events & BUTTON_EVENT) {
                LOG_DBG("Trigger switch to pressure_read state.");

                /* Read baseline atm pressure */
                pressure_mmhg.atm = 0;
                int ret = read_pressure_sensor(pressure_in, &pressure_mmhg);
                if (ret == -1) {
                        LOG_ERR("Could not read pressure sensor.");
                        smf_set_state(SMF_CTX(&state_obj), &user_states[error]);
                } else {
                        pressure_mmhg.atm = pressure_mmhg.buffer;
                        LOG_INF("Baseline atmospheric pressure: %d mmhg", pressure_mmhg.atm);    
                }

                k_timer_stop(&measure_motion_timer);

                // gpio_pin_set_dt(&power2, 1);
                gpio_pin_set_dt(&pump, 1);
                gpio_pin_set_dt(&solenoid1, 1);
                gpio_pin_set_dt(&solenoid2, 1);


                smf_set_state(SMF_CTX(&state_obj), &user_states[pressure_inflate]);
                state_obj.events = 0;

        }
}

/**** STATE: PRESSURE INFLATE ******/
static void pressure_inflate_entry(void *o) {
	LOG_INF("STATE: PRESSURE INFLATE");
        k_event_clear(&state_obj.smf_event, BUTTON_EVENT);
        gpio_pin_set_dt(&greenled, 0);
        gpio_pin_set_dt(&blueled, 1);
        k_timer_start(&measure_pressure_timer, K_MSEC(1), K_MSEC(1));

}

static void pressure_inflate_run(void *o) {

        /* Block until an event is detected */
        state_obj.events = k_event_wait(&state_obj.smf_event, STOP_INFLATION, false, K_NO_WAIT);
        if (state_obj.events & STOP_INFLATION) {
                gpio_pin_set_dt(&pump, 0);  //turns pump off
                LOG_INF("pump turned off");
                k_event_clear(&state_obj.smf_event, STOP_INFLATION);
                k_timer_stop(&measure_pressure_timer);
                smf_set_state(SMF_CTX(&state_obj), &user_states[pressure_deflate]);

        }      

}


/**** STATE: PRESSURE DEFLATE ******/
static void pressure_deflate_entry(void *o) {
	LOG_INF("STATE: PRESSURE DEFLATE");
        gpio_pin_set_dt(&greenled, 1);
        gpio_pin_set_dt(&blueled, 1);
        gpio_pin_set_dt(&redled, 1);
        k_event_clear(&state_obj.smf_event, FULL_DEFLATE);

        pressure_mmhg.max_count=0;
        k_timer_start(&measure_deflate_timer, K_MSEC(1), K_MSEC(1));

}

static void pressure_deflate_run(void *o) {

        /* Block until an event is detected */
        state_obj.events = k_event_wait(&state_obj.smf_event, FULL_DEFLATE, false, K_NO_WAIT);
        if (state_obj.events & FULL_DEFLATE) {
                k_timer_stop(&measure_deflate_timer);
                LOG_INF("everything turned off");
                gpio_pin_set_dt(&pump, 0);  //turns pump off
                gpio_pin_set_dt(&solenoid1, 0);  //turns pump off
                gpio_pin_set_dt(&solenoid2, 0);  //turns pump off
                k_event_clear(&state_obj.smf_event, FULL_DEFLATE);
                smf_set_state(SMF_CTX(&state_obj), &user_states[idle]);

        }      

}

static void pressure_deflate_exit(void *o) {
	LOG_INF("STATE: PRESSURE DEFLATE (Exit)");
   
        k_msleep(1000);
        printk("Pressure Data:");
        for (int i=0; i<TOTAL_SIZE; i++){
                printk("%d,",pressure_mmhg.pressure_curve_inflation[i]);
                k_msleep(10);
        }
        printk("\n");
}



/**** STATE: ERROR ****/
static void error_entry(void *o) {
	LOG_INF("STATE: ERROR");
	gpio_pin_set_dt(&redled, 1);
	gpio_pin_set_dt(&greenled, 0);
	gpio_pin_set_dt(&blueled, 0);
}

static void error_run(void *o){
        state_obj.events = k_event_wait(&state_obj.smf_event, BUTTON_EVENT, false, K_NO_WAIT);
        if (state_obj.events & BUTTON_EVENT) {
                LOG_DBG("Trigger switch to pressure_read state.");
                smf_set_state(SMF_CTX(&state_obj), &user_states[idle]);
                state_obj.events = 0;

        }
}

/* This whole state is just for testing, we'll be polling for accelerometer data in idle */
/**** STATE: ACCEL ******/
static void accel_entry(void *o) {
}

static void accel_run(void *o) {
        LOG_DBG("In accel state yippee");
        get_accel_vals();
        k_msleep(1000);
        // smf_set_state(SMF_CTX(&state_obj), &user_states[idle]); 
}

static void accel_exit(void *o) {
}



void main(void) {

    int err;

    /*Initialize Event Framework*/
    k_event_init(&state_obj.smf_event);

    /*Set Initial State*/
    smf_set_initial(SMF_CTX(&state_obj), &user_states[init]);

    /*Can refactor this to have work queues later*/
    while (1) {
        
        err = smf_run_state(SMF_CTX(&state_obj));
        if (err) {
            break;
        }

        k_msleep(SLEEP_TIME_MS);
    }
}

int check_interfaces(void) {

    /* Check if all devices are ready to go */
        if (!device_is_ready(pressure_in)) {
		LOG_ERR("MPR pressure sensor %s is not ready", pressure_in->name);
		return -1;
	} else {
                LOG_INF("MPR pressure sensor %s is ready", pressure_in->name);
        }
    
	return 0;
}


int check_gpio_interfaces(void) {
        if (!device_is_ready(blueled.port)) {
		LOG_ERR("gpio0 interface not ready.");
		return -1;
	}

    return 0;
}

int config_leds(void) {

        int ret;

	ret = gpio_pin_configure_dt(&redled, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}

        ret = gpio_pin_configure_dt(&greenled, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}
        ret = gpio_pin_configure_dt(&blueled, GPIO_OUTPUT_ACTIVE);
	if (ret < 0) {
		return ret;
	}
        ret = gpio_pin_configure_dt(&power2, GPIO_OUTPUT_INACTIVE);
	if (ret < 0) {
		return ret;
	}
        ret = gpio_pin_configure_dt(&pump, GPIO_OUTPUT_INACTIVE);
	if (ret < 0) {
		return ret;
	}
        ret = gpio_pin_configure_dt(&solenoid1, GPIO_OUTPUT_INACTIVE);
	if (ret < 0) {
		return ret;
	}
        ret = gpio_pin_configure_dt(&solenoid2, GPIO_OUTPUT_INACTIVE);
	if (ret < 0) {
		return ret;
	}
        return 0;
}

int config_buttons(void) {
        int err;

    /* Initialize Button GPIO pins to Inputs */
	err = gpio_pin_configure_dt(&button, GPIO_INPUT);
	if (err < 0) {
		LOG_ERR("Cannot configure adc button pin.");
                return err;
	}
        return 0;
}

int read_pressure_sensor(const struct device *pressure_in, struct Pressure *pressure_mmhg) {

    int err;
    // float pressure_kPa = 0;
    // float raw_pressure_kPa = 0;
    float raw_pressure_mmhg = 0;
    struct sensor_value pressure_vals = {.val1 = 0, .val2 = 0};


        //if we haven't taken a baseline reading yet, use the second part to set the atm pressure
    if (pressure_mmhg->atm != 0){
            err = sensor_sample_fetch(pressure_in);
            if (err != 0) {
                LOG_ERR("MPR sensor_sample_fetch error: %d", err);
                return -9999;
            }
            else {
                err = sensor_channel_get(pressure_in, SENSOR_CHAN_PRESS, &pressure_vals);
                if (err != 0) {
                    LOG_ERR("MPR sensor_channel_get error: %d", err);
                    return -9999;
                }
            }
            
            // data returned in mmhg - for some reason it is around 760 :(
            raw_pressure_mmhg = 7.500638 * ((float)pressure_vals.val1 + (float)pressure_vals.val2/1000000);
        //     LOG_DBG("Raw Pressure (kPa): %d", (int)raw_pressure_mmhg);
    } else {
            err = sensor_sample_fetch(pressure_in);
            if (err != 0) {
                LOG_ERR("MPR sensor_sample_fetch error: %d", err);
                return -9999;
            }
            else {
                err = sensor_channel_get(pressure_in, SENSOR_CHAN_PRESS, &pressure_vals);
                if (err != 0) {
                    LOG_ERR("MPR sensor_channel_get error: %d", err);
                    return -9999;
                }
            }
            
            // data returned in kPa
            raw_pressure_mmhg =  7.500638 * ((float)pressure_vals.val1 + (float)pressure_vals.val2/1000000);
            LOG_DBG("Raw Pressure (mmhg): %d", (int)raw_pressure_mmhg);
            pressure_mmhg->buffer = (int)raw_pressure_mmhg - pressure_mmhg->atm;
            LOG_DBG("ambient Pressure (mmhg): %d", pressure_mmhg->buffer);
            return 0;
    }


    pressure_mmhg->buffer = (int)raw_pressure_mmhg - pressure_mmhg->atm;
//     LOG_DBG("Mean Relative Pressure (mmhg): %d", pressure_mmhg->buffer);

    return pressure_mmhg->buffer;
}

static int print_accels(const struct device *dev, size_t index)
{
    int ret;
    struct sensor_value accel[3];

    ret = sensor_sample_fetch(dev);
    if (ret < 0) {
        printk("%s: sensor_sample_fetch() failed: %d\n", dev->name, ret);
        return ret;
    }

    for (size_t i = 0; i < ARRAY_SIZE(channels); i++) {
        ret = sensor_channel_get(dev, channels[i], &accel[i]);
        if (ret < 0) {
            printk("%s: sensor_channel_get(%c) failed: %d\n", dev->name, 'X' + i, ret);
            return ret;
        }
    }

    // Save accelerometer values in the global array
    accel_data[index].x = sensor_value_to_double(&accel[0]);
    accel_data[index].y = sensor_value_to_double(&accel[1]);
    accel_data[index].z = sensor_value_to_double(&accel[2]);

    return 0;
}

int get_accel_vals(void)
{
    int ret;

    for (size_t i = 0; i < ARRAY_SIZE(sensors); i++) {
        if (!device_is_ready(sensors[i])) {
            printk("sensor: device %s not ready.\n", sensors[i]->name);
            return 0;
        }
    }

    for (size_t i = 0; i < ARRAY_SIZE(sensors); i++) {
        ret = print_accels(sensors[i], i);
        if (ret < 0) {
            return 0;
        }
    }

    return 0;
}



/**** STATE TABLE ****/
static const struct smf_state user_states[] = {
        [init] = SMF_CREATE_STATE(init_entry, init_run, init_exit),
        [idle] = SMF_CREATE_STATE(idle_entry, idle_run, NULL),
        [accel] = SMF_CREATE_STATE(accel_entry, accel_run, accel_exit), // Placeholder, this will go into idle once we know it works
        [pressure_inflate] = SMF_CREATE_STATE(pressure_inflate_entry, pressure_inflate_run, NULL),
        [pressure_deflate] = SMF_CREATE_STATE(pressure_deflate_entry, pressure_deflate_run, pressure_deflate_exit),

	[error] = SMF_CREATE_STATE(error_entry, error_run, NULL)
};
/*********************/